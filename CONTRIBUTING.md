What’s in it for me?
Get early access to all the latest features before they are officially released
Test your compatibility to make sure your projects are ready to be upgraded
Join the experts to share insights with experienced members of the Unity community
Win cool prizes—beta testers are automatically entered into our sweepstakes
Influence the future of Unity with surveys, feedback and the chance to be invited to roundtables
Be part of an elite group that gets special benefits such as discounts and invites to special events
